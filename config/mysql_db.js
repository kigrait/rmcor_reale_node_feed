const { createPool } = require('mysql2');

const pool = createPool({
    host: "localhost",
    user: "root",
    password: "root",
    database: "rmcor_mysql_database",
    connectionLimit: 10
})

pool.query('select * from login', (err, result, fields) => {
    if(err) {
        return console.log(err);
    }
    return console.log(result);
})

// let mysql = require('mysql2');

// let connection = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'root',
//     password : 'root',
//     database: "rmcor_mysql_database",
//     connectionLimit: 10
// });


// connection.connect(function(err) {
//     if (err) {
//       return console.error('error: ' + err.message);
//     }
  
//     console.log('Connected to the MySQL server.');
//   });

